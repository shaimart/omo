package cz.cvut.k36.omo.hw.hw01;

public class Homework1 {
    private static int counterI = 0;
    private int counterH = 0;

    public Homework1() {

    }

    public boolean f() {
        return true;
    }

    public static boolean g() {
        return false;
    }

    public int h() {
       return ++counterH;
    }

    public int i() {
        return ++counterI;
    }

}
